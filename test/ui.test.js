const puppeteer = require('puppeteer');
const url = 'http://localhost:3000'
const openBrowser = true; 

// 1 - example 
test('[Content] Login page title should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
    expect(title).toBe('Login');
    await browser.close();
})


// 2 
test('[Content] Login page join button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.waitForSelector('body > div > form > div:nth-child(4) > button');

    let button = await page.evaluate(() => {
        return document.querySelector("body > div > form > div:nth-child(4) > button").innerHTML;
    });


    expect(button).toBe('Join');
    await browser.close();
})

// 3 - example 
test('[Screenshot] Login page', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.screenshot({path: './test/screenshots/login.png'});
    await browser.close();
})

// 4
test('[Screenshot] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 500}); 
    await page.waitForSelector('#messages > li > div.message__body > p');

    let welcome = await page.evaluate(() => {
        return document.querySelector("#messages > li > div.message__body > p").innerHTML;
    });
    expect(welcome).toBe('Hi John, Welcome to the chat app');
    await browser.close();
})

// 5
test('[Screenshot] Error message when you login without user and room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.keyboard.press('Enter', {delay: 1000}); 
    await page.screenshot({path: './test/screenshots/error.png'});
    await browser.close();

})

// 6
test('[Content] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 1000}); 
    await page.screenshot({path: './test/screenshots/welcome.png'});
    await browser.close();
})

// 7 - example
test('[Behavior] Type user name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('John');

    await browser.close();
})

// 8
test('[Behavior] Type room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R8', 100);
    await page.waitForSelector('body > div > form > div:nth-child(3) > input[type=text]');
    let roomName = await page.evaluate(() => {
        return document.querySelector("body > div > form > div:nth-child(3) > input[type=text]").value;
    });
    expect(roomName).toBe('R8');
    await browser.close();
})

// 9 - example
test('[Behavior] Login', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('John');
    
    await browser.close();
})

// 10
test('[Behavior] Login 2 users', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R10', {delay: 100});
    await page.keyboard.press('Enter', {delay: 200}); 

    await page.waitForSelector('#users > ol > li:nth-child(1)');    
    let member1 = await page.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });
    const page2 = await browser.newPage();
    await page2.goto(url);

    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mary', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R10', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 200}); 

    await page2.waitForSelector('#users > ol > li:nth-child(2)');    
    let member2 = await page.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });
    expect(member1).toBe('John');
    expect(member2).toBe('Mary');
    
    await browser.close();
})

// 11
test('[Content] The "Send" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R11', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector("#message-form > button");
    let button = await page.evaluate(() => {
        return document.querySelector("#message-form > button").innerHTML;
    });

    expect(button).toBe("Send");
    await browser.close();
})

// 12
test('[Behavior] Send a message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R12', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitForSelector('#message-form > button');

    await page.type('#message-form > input[type=text]', 'Hi', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});
    await page.waitForSelector('#messages > li:last-child > div.message__body > p');

    let message = await page.evaluate(() => {
        return document.querySelector("#messages > li:last-child > div.message__body > p").innerHTML;
    });

    expect(message).toBe("Hi");
    await browser.close();

})

// 13
test('[Behavior] John says "Hi" and Mike says "Hello"', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R13', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#message-form > button');

    


    const page2 = await browser.newPage();
    await page2.goto(url);

    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R13', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100}); 

    await page2.waitForSelector('#message-form > button'); 


    await page.type('#message-form > input[type=text]', 'Hi', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100})
    await page.waitForSelector('#messages > li:last-child > div.message__body > p');  
    await page2.type('#message-form > input[type=text]', 'Hello', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100})
    await page2.waitForSelector('#messages > li:last-child > div.message__body > p');

    let user1 = await page.evaluate(() => {
        return document.querySelector("#messages > li:nth-last-child(2) > div.message__title > h4").innerHTML;
    });
    let message1 = await page.evaluate(() => {
        return document.querySelector("#messages > li:nth-last-child(2) > div.message__body > p").innerHTML;
    });

    let user2 = await page.evaluate(() => {
        return document.querySelector("#messages > li:nth-last-child(1) > div.message__title > h4").innerHTML;
    });
    let message2 = await page.evaluate(() => {
        return document.querySelector("#messages > li:nth-last-child(1) > div.message__body > p").innerHTML;
    });

    expect(user1).toBe("John");
    expect(message1).toBe("Hi");
    expect(user2).toBe("Mike");
    expect(message2).toBe("Hello");
    
    await browser.close();
})

// 14
test('[Content] The "Send location" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R14', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector("#send-location");
    let button = await page.evaluate(() => {
        return document.querySelector("#send-location").innerHTML;
    });

    expect(button).toBe("Send location");
    await browser.close();
})

// 15
test('[Behavior] Send a location message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    const context = browser.defaultBrowserContext();
    context.clearPermissionOverrides();
    context.overridePermissions(new URL(url).origin, ['geolocation']);

    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R15', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitForSelector("#send-location");
    await page.$eval('#send-location', el => el.click());
    await page.waitForSelector("#send-location")
    let button = await page.evaluate(() => {
        return document.querySelector("#send-location").innerHTML;
    });
    
    expect(button).toBe("Sending location...");
    await browser.close();

})
